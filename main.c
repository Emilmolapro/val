#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "palval.h"


int main(int argc, char* argv[])
{
    int length,i=0;
    char input[MAXINPUT] = "";


    printf("Please enter a phone number:\n\n+36-");//check number of arguments
    if(argc!=2){
        printf("\nInvalid number of arguments\n");
        exit(EXIT_FAILURE);

    }

    strcpy(input, argv[1]);//check minimal length
    length=strlen(input);
    if(length<MININPUT){
        printf("\nInvalid phone number - not enough characters");
        exit(EXIT_FAILURE);
    }

    printf("%s", input);//check for mobile or line
    for (i=0;i<length; i++){
        if (!isdigit(input[i]))
        {
            printf ("\nInvalid phone number - please enter up to 9 digits & digits only\n");
            exit(EXIT_FAILURE);
        }
    }
    if(length<MAXINPUT){
        printf("\nValid line phone number");}
            else{
                    printf("\nValid mobile phone number");
    }
    return 0;

}
